from django import forms # This is what is needed for forms. 
from receipts.models import Receipt

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget= forms.PasswordInput())