from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.
#Question 1: Create a view that will get all of the instances of the 
#            Receipt model and put them in the context for the template.
# my take - Unsure about what any of these mean. 
def receipt_list(request):
    all_receipts = Receipt.objects.all()     # objects is the manager, it will give all the receipts
    context = {
        "all_receipts": all_receipts 
    } # everyting that need to be in the list.html page need to be in the contxt which is simply a holder. 
    return render(request, "receipts/list.html", context)

