from django.contrib import admin 
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    pass
    

# The way I registered them is a bit different. 

#1. I imported the three classes from receipts.models/ via 

    # from receipts.models import ExpenseCategory, Account, Receipt

# 2. Type Modeladmin and a link will open. click it and it should display the following. 

#     @admin.register()
#     class Admin(admin.ModelAdmin):

#     - Add the class in incided the parenthesis for first part and for the second part
#       Insert the name of class Prior to Admin. 

# add "pass" after each of the colons. 

        
    