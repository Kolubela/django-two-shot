from django.db import models
from django.conf import settings         # This is how you will import the settings for django. in it lies the USER module needed for this part of the project. 

# Create your models here.
class ExpenseCategory(models.Model): 
    name = models.CharField(max_length=50)
    owner= models.ForeignKey(
        settings.AUTH_USER_MODEL,       # This is how we add the USER. Note that user is refering to a module in django which we don't see.
        related_name="categories",      # This is the way for us to find out what the lowerlimit is in this case its Expesecategory when refering to the upperlimit whihc is USER
        on_delete=models.CASCADE,       # if this thing that i am referencing in this case it the uplimit called USER is deleted, then delete the enire class ExpenseCategory 
        )

class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner =models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts", 
        on_delete=models.CASCADE,
        )

class Receipt(models.Model): 
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(
        max_digits=10, 
        decimal_places=3,
        )
    tax = models.DecimalField(
        decimal_places=3,
        max_digits=10,
        )
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL, 
        related_name="receipts", 
        on_delete=models.CASCADE,
        )
    category = models.ForeignKey(
        "ExpenseCategory", 
        related_name="receipts", 
        on_delete=models.CASCADE,
        )
    account = models.ForeignKey(
        "Account",  # All make the upperlimit a string with execption of settings.AUTH_USER_MODEL
        related_name="receipts",
        null=True, 
        on_delete=models.CASCADE,
        )


                            

