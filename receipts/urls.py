from django.urls import path 
from receipts.views import receipt_list

urlpatterns = [
    path("", receipt_list, name="home") 
    
      # name is just the name we will use to refer to the page that has receipt_list in it. 
]




